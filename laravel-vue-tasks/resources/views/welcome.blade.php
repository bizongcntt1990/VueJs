<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <title>Vue Js Demo</title>
</head>
<body>
  <h1>Laravel Vue Demo</h1>

  <div id="app">
    <article v-repeat="messages">
      <h3>@{{ name }}</h3>
      <div>@{{ message }}</div>
    </article>
  </div>

  <!-- JS -->
  <script src="/js/vendor.js"></script>
  <script src="/js/vue-demo.js"></script>
</body>