<html>
  <head>
    <title>Laravel</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  </head>
  <body>
    <div class="container">
      <tasks></tasks>
    </div>

    <template id="tasks-template">
      <h1>My Tasks</h1>
      <ul class="list-group">
        <li class="list-group-item" v-for="task in list">
            @{{ task.body }}
            <strong @click="deleteTask(task)">X</strong>
        </li>
      </ul>
    </template>

    <script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.8/vue.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.1.17/vue-resource.min.js"></script>
    <script type="text/javascript">
      var app = new Vue({
          el: '.container',
          components : {
            tasks : {
              template: "#tasks-template",
              data : function() {
                return {
                  list: []
                };
              },
              created() {
                this.fetchTaskList();
              },
              methods : {
                fetchTaskList : function() {
                  var resource = this.$resource('api/tasks/:id');
                  resource.get(function(tasks) {
                    this.list = tasks;
                  }.bind(this));
                },
                deleteTask : function(task) {
                  this.list.$remove(task);
                }
              }
            }
          }
        });
    </script>
  </body>
</html>
