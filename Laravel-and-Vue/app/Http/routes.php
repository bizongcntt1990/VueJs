<?php

use App\Message;
use App\Task;

get('guestbook', function() {
    return view('guestbook');
});

// API

get('api/messages', function() {
    return Message::all();
});

post('api/messages', function() {
    return Message::create(Request::all());
});

get('/', function() {
    return View('welcome');
});

get('api/tasks', function() {
    return Task::all();
});